<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Branch;

class BranchController extends Controller
{
    public function index() {
	return view('branches.index', ['branches' => Branch::paginate(20)]);
    }


    public function show(Branch $branch) {
    }

    public function store(Request $request) {
	Branch::create($request->all());
	return redirect()->route('branches.create');
    }

    public function create() {
	return view('branches.create');
    }

    public function update(Branch $branch, Request $request) {
	$branch->fill($request->all());
	$branch->save();
	return redirect()->route('branches.edit', ['branch' => $branch]);
    }

    public function edit(Branch $branch) {
	return view('branches.edit', ['branch' => $branch]);
    }

    public function destroy(Branch $branch) {
	$branch->delete();
	return redirect()->route('branches.index');
    }
    //
}
