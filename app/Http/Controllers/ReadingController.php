<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Reading;
use App\Branch;
use App\Location;

class ReadingController extends Controller
{
    public function index(Branch $branch = null) {
	if($branch) {
	    $readings = $branch->readings()->orderBy('id', 'desc')->paginate(50);
	} else {
	    $readings = Reading::orderBy('id', 'desc')->paginate(50);
	}

	return view('readings.index', ['readings' => $readings, 'branches' => Branch::all()]);
    }

    public function logLocation(Request $request) {
	$data = json_decode($request->data);
	foreach($data as $location) {
	    Location::create($location);
	}
    }

    public function logBranch(Request $request) {

    }

    public function logTemp(Request $request) {
	
    }
}
