<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/readings', 'ReadingController@index')->name('readings');

Route::post('/logTemp', 'ReadingController@logTemp')->name('log.temp');
Route::post('/logLocation', 'ReadingController@logLocation')->name('log.location');
Route::post('/logBranch', 'ReadingController@logBranch')->name('log.branch');
